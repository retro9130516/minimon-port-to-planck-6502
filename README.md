## Port of Floobydust's MicroMon 1.2 to Planck 6502

This is a port of the
[Micromon](https://github.com/floobydust/Micromon-V1.2) project
created by Floobydust so that it will run on the [Planck 6502
SBC](https://planck6502.com/). I'm making this port to learn more
about 6502 assembly language and so there are bound to be errors
or problems. 

I've adapted the source code to compile using the
[ca65](https://cc65.github.io/doc/ca65.html) assembler. I've
replaced the "org" directives with the ".segment" command that
ca65/ld65 seems to prefer. Other changes include replacing `.db`
with `.byte` and `.dw` with `.word`.

I also added `.PC02` to turn on support for 65C02 op codes and
added `.FEATURE labels_without_colons`, which allows labels
without colons. I also commented out the printing related items
at the top of the file.

Because the Planck uses a WDC 65C51, which has a well-known
transmit bug (search 6502.org for extensive discussions of the
issue), it's not possible to use interrupt-driven data
transmission. To work around this bug, I've substituted
Floobydust's delay-based routines from the 6502.org for
transmission; however, the receive function is still
interrupt-driven and this may be something that can be ported
back over to the Planck.

If you compile this for your Planck, you may want to adjust the
`ACIA_DELAY` variable. My Planck is running with 1 wait state so
I set the delay to 6 (representing a 6 Mhz processor speed). The
ROM assumes that the user will connect at 19,200 N-8-1. If you
change it to 115200, then you probably want to change the delay
routine so that it consumes about 100 cycles per loop, rather
than 600.

I've also included some basic shell scripts for building the bin
file and for burning it to ROM.

## Micro monitor functionality

The monitor packs a lot of functionality. Here is a list of the
keys that can be used. In addition, the NMI button on the Planck
will trigger a diagnostic save of computer data. 

- [CNTRL-R] Reset
- ( Init Macro
- ) Run Macro
- A Display/Edit A register
- C Compare memory block
- D Display Memory contents in HEX/TEXT
- F Fill memory block
- G Go execute at <addr>
- H Hex byte string search
- M Move memory block
- P Display/Edit CPU status reg
- R Display Registers
- S Display/Edit stack pointer
- T Text character string search
- X Display/Edit X register
- Y Display/Edit Y register

Much more can be learned from studying the source code.

Again, I didn't create any of this. This is all great work by
Floobydust, who made MicroMon and by Jonathan Foucher, who
created the Planck computer
